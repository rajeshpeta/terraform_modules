resource "aws_key_pair"  "mykey" {
    key_name = "mykey"
    public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"

}

resource "aws_instance" "Terraform_Instance" {
    ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.mykey.key_name}"

    provisioner "local-exec" {
        command = "echo ${aws_instance.Terraform_Instance.private_ip} >> private_ips.txt"
    }

    provisioner "file" {
        source = "nginx_script.sh"
        destination = "/tmp/nginx_script.sh"
    }
    provisioner "remote-exec" {
        inline = [
            "chmod +x /tmp/nginx_script.sh",
            "sudo /tmp/nginx_script.sh"
        ]
    }
    connection {
        host        = coalesce(self.public_ip, self.private_ip)
        type        = "ssh"
        user = "${var.INSTANCE_USERNAME}"
        private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
    }
    }
output "ip" {
    value = aws_instance.Terraform_Instance.public_ip
}